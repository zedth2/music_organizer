#!/bin/bash

dest='/run/media/zac/extras/Music/music'
function organize {
    IFS=$'\n'; for i in $(find . -name "*.opus"); do
	title="$(opusinfo "$i" | grep -m1 TITLE= | cut -d= -f2 | tr ' ' '_' | tr -d '/')";
	artist="$(opusinfo "$i" | grep -m1 ARTIST= | cut -d= -f2 | tr ' ' '_')";
	album="$(opusinfo "$i" | grep -m1 ALBUM= | cut -d= -f2 | tr ' ' '_')";
	d="$dest/$artist/$album"
	dir="$(dirname $i)"
	echo "Working Title" "$d/$title.opus";
	echo mkdir -p "$d";
	mkdir -p "$d";
	echo mv "$i" "$d/$title.opus";
	mv "$i" "$d/$title.opus";
	if [ ! -f "$d/cover.jpg" -a -f "$dir/cover.jpg" ] ; then
	    echo cp "$dir/cover.jpg" "$d/cover.jpg"
	    cp "$dir/cover.jpg" "$d/cover.jpg"
	fi
	echo
    done
}


function to_opus {

 IFS=$'\n'; for i in $(find . -name "*.flac"); do
   b="$(dirname ${i})/$(basename ${i} .flac)";
   echo ${b};
   opusenc ./${b}.flac ./${b}.opus;
done

}

function extract {
    IFS=$'\n'; for i in $(find . -name "*.zip"); do
	b="$(dirname ${i})/$(basename ${i} .zip)"
	echo ${b};
	mkdir -p ${b};
	unzip ${i} -d ${b}
    done
}


if [ "$1" == "-t" ] ; then
    to_opus $*
elif [ "$1" == "-o" ] ; then
    organize $*
elif [ "$1" == "-z" ]; then
    extract $*
fi
